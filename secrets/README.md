# Credentials

Contains SSH private keys, Gitlab Personal Access Tokens and passwords

Please rename variables.env.sample to WhateverYouWant.env and include it as environment variables in your setup.

Refer to ../casc/credentials.yaml for full details
