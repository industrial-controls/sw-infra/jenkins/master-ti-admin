# TI Administration Development Jenkins Master image

Jenkins Master image and configuration for TI Administration (TIM, NXCALS)

<img src="http://jenkins-ci.org/sites/default/files/jenkins_logo.png"/>



# How to deploy

## Pre-requisite : How to setup Openshift for automated deployment

An Openshift service account can be used to deploy and configure the application. The service account is limited to a given Openshift project (=namespace).
Use the following commands to register a service account - for instance, we use an Openshift project called "test-jenkins-project" and a service account "jenkins-deployer".

```bash
oc create serviceaccount jenkins-deployer
oc policy add-role-to-user admin system:serviceaccount:test-jenkins-project:jenkins-deployer
oc serviceaccounts get-token jenkins-deployer
```
This should display the authentication token that you can now use to authenticate :

```bash
oc login <YOUR_SERVER> --token=<YOUR_NEW_TOKEN>
```

## Step 1 - Optionally push images to the Gitlab registry

New images are pushed by Gitlab CI automatically. You can also build and push them manually.

```bash
export VERSION=<NEW_VERSION>
docker build -t https://gitlab.cern.ch/industrial-controls/sw-infra/jenkins/master-ti-admin:$VERSION .
docker push https://gitlab.cern.ch/industrial-controls/sw-infra/jenkins/master-ti-admin:$VERSION

```

## Step 2 - Deploying required secrets and config maps

These includes : SSH keys for git operations, passwords for legacy authentication, Gitlab personal access tokens for API access.

```bash
oc login <your_credentials>
oc project <your_project_name>
oc create secret generic jenkins-ssh-keys \
    --from-file=secrets/keys/
    --type=kubernetes.io/ssh-auth
oc create secret generic bjenkins-secrets \
    --type=opaque \
    --from-literal=BJENKINS_PASSWORD=<password> \
    --from-literal=BJENKINS_GITLAB_PAT=<token>
oc create configmap casc-config \
    --from-file=casc/
```

You must also add a configuration map for environment variables, as they might differ between your TEST (develop) and PROD (master) environments :
```
oc create configmap env-config \
    --from-env-file=deployment/develop.env
```
or
```
oc create configmap env-config \
    --from-env-file=jenkins.env=deployment/master.env
```

## Step 3 - Process OpenShift templates

For example, to TEST servers :
```bash
oc process --ignore-unknown-parameters --param-file=deployment/develop.env -f deployment/templates/deployment.yml --local=true | oc apply -f - --server=https://openshift-dev.cern.ch --token=$OPENSHIFT_TOKEN
oc process --ignore-unknown-parameters --param-file=deployment/develop.env -f deployment/templates/service.yml --local=true | oc apply -f - --server=https://openshift-dev.cern.ch --token=$OPENSHIFT_TOKEN
```

Or to PROD servers :
```bash
oc process --ignore-unknown-parameters --param-file=deployment/master.env -f deployment/templates/deployment.yml --local=true | oc apply -f - --server=https://openshift.cern.ch --token=$OPENSHIFT_TOKEN
oc process --ignore-unknown-parameters --param-file=deployment/master.env -f deployment/templates/service.yml --local=true | oc apply -f - --server=https://openshift.cern.ch --token=$OPENSHIFT_TOKEN
```

# How to develop

```bash
docker run -ti --rm --net=host  -v `pwd`/casc:/var/lib/jenkins/casc_configs -v /tmp/jenkins_home:/var/jenkins_home:z -v `pwd`/secrets:/secrets -e SECRETS_DIRECTORY=/secrets/ --env-file=`pwd`/secrets/bjenkins.env gitlab-registry.cern.ch/industrial-controls/sw-infra/jenkins/master-ti-admin
```


## Openshift URL



