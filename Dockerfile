FROM gitlab-registry.cern.ch/industrial-controls/sw-infra/jenkins/master-master:latest

# Install specific plugins, e.g. :
USER root
RUN jenkins-plugin-cli --plugins matrix-auth:2.6.7 ssh-slaves:1.31.5 ssh-agent:1.22 \
    analysis-model-api:10.2.5 ant:1.11 build-failure-analyzer:2.0.0 build-timeout:1.19 \
    config-file-provider:3.8.0 email-ext:2.83 envinject:2.4.0 git-parameter:0.9.13 \
    gitlab-plugin:1.5.12 groovy:2.2 hudson-pview-plugin:1.8 m2release:0.16.2 \
    maven-info:0.2.0 maven-plugin:3.10 \
    permissive-script-security:0.4 PrioritySorter:3.6.0 promoted-builds:3.10 \
    promoted-builds-simple:1.9 rebuild:1.32 release:2.11 sitemonitor:0.6 timestamper:1.12 \
    token-macro:2.15 view-job-filters:2.1.1 warnings-ng:9.1.0 ws-cleanup:0.37

# REMOVED mail-watcher-plugin:1.16

USER 1001


ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/libexec/run"]

